# simple_testing

Just a small self-made testing lib.

### **Content**

- What is this for?
- License
- Usage instructions
- Code usage example

---

## What is this for?

This little header presents a small class that can easyly be used to make some simple code testing
by comparing 2 results are equal. Also, using strings (or not ;) ) it can print test results from cout
or in a named file.

---

## License

This software us licensed under License GPL-3.0-only.

    Copyright Joseba Martinez 2019  (josebam@protonmail.com).

---

## Usage instructions

1. Include the simpletest.h headder in your project
2. Create a class instance with the type of your choice and a name
3. Add tests until you want, with yourClass.test( result1, result2, testName )
4. Either .print() or .print_to_txt( fileName ) to gather the results
5. For multiple uses, the class can be .reset( newName ), clearing all previous results

> **Note:** if empty names are used, automatic countered will appear!
> **Note 2:** print_to_txt() will add ".result.txt" to the file name.
> **Note 3:** add_line will add a commented line to printed result.

---

## Code usage example


```c++

#include "type_nreal.h"
#include "simpletest.h"

/* Nreal is a fixed point numbers implementation based on int type */

using namespace TESTING;    // for test_unit<Nreal> class
using namespace NUM;        // for Nreal type

int main()
{
    // Basic algebra tests
    test_unit<Nreal> t1("Basic arithmetic oeprations tests"); // test class creation
        // Addition tests
    // Compare value 1 against value 2: t1.test( Val1, Val2 ),"Line name");
    t1.test(Nreal(1)+Nreal(1), Nreal(2),"Addition"); // the wrong one ;)
    t1.test(Nreal(0)+Nreal(0), Nreal(0),"Add 0"); // 0+0=0
    t1.test(Nreal(-1.234)+Nreal(1.234), Nreal(0),"Add opposit"); // -A+A=0
    t1.test(Nreal(9999999999999)+Nreal(1), Nreal(999999999999999999),"Upper overflow limit test"); // Up_limit+1=Up_limit


    t1.print();

    return 0;
}

```

### Output of this example:

```

--+ Starting test: Basic arithmetic oeprations tests +--

Addition ... ERROR
Add 0 ... OK
Add negative ... OK
Limit overflow ... OK

```
