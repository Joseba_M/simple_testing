#-------------------------------------------------
#
# Project created by QtCreator 2019-10-12T14:12:17
#
#-------------------------------------------------

QT       -= core gui

TARGET = simpleTest
TEMPLATE = lib
CONFIG += staticlib

SOURCES +=

HEADERS += \
        simpletest.h

DISTFILES += \
    ../README.md \
    COPYNG.txt
