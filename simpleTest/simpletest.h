/*
 * License GPL-3.0-only
 *
 * Copyright Joseba Martinez 2019 (josebam@protonmail.com)
 *
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, with version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef SIMPLETEST_H
#define SIMPLETEST_H

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace TESTING {

template<class T>
class test_unit {
public:
    test_unit(std::string s)
    {
        if (s.empty()) { s="Unnamed Test"; }
        count=0;
        name=s;
        txt="\n\t--+ Starting test: "+s+" +--\n\n";
    }

    void test(const T &a, const T &b, std::string s)
    {
        ++count;
        if (s.empty()) { s="Test "+std::to_string(count); }
        if (a==b)   txt+=s+" ... OK\n";
        else        txt+=s+" ... ERROR\n";
    }

    void print()
    {
        std::cout<<txt<<std::endl;
    }

    void print_to_txt(std::string s)
    {
        if (s.empty()) { s=name; }
        s+=+".result.txt";
        std::ofstream f(s, std::ios_base::trunc);
        f<<txt<<std::endl;
    }

    void reset(std::string s)
    {
        if (s.empty()) { s="Unnamed Test"; }
        count=0;
        txt.clear();
        name=s;
    }

    void add_line(const std::string &s)
    {
        txt+="\n\t// "+s+"\n";
    }

private:
    size_t count;
    std::string txt;
    std::string name;

};


} // namespace TESTING


#endif // SIMPLETEST_H
